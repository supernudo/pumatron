#include <lpc213x.h>
#include <stdlib.h>
#include <stdio.h>
#include "target.h"
#include "Declaraciones.h"
#include "PID.h"
#include "s1d15g00.h"
#include <stdint.h>

//#define CIRCUITO_3_LINEAS

char debug_print_buff[128];
int debug_print_counter = 0;
int debug_pid = 0;

uint16_t virtual_line_position;

/********************** ALGORITMO PID **********************/

int get_line_position_error(void)
{
	sensors_average = 0;
	sensors_sum = 0;
	negros = 0;

	for(i = 0; i < S; i++)
	{
		if(ADCresult[i] < 100)
			ADCresult[i] = 0;
		else if(ADCresult[i] > umbral[i])
			negros++;

		sensors_average += ADCresult[i]*(i+1)*1000;
		sensors_sum += ADCresult[i];
	}

	/* TODO: negros var is used for track change, must have the same behaviour */

	if(negros == 0)
	{
		if(giro == IZQ)
			position = 1000;
		else // giro == DER
			position = S*1000;
	}
	else // sensors_sum != 0
		position = sensors_average / sensors_sum;

		return (position - (S+1)*1000/2);
}

/* TODO: use position sign to get the line where the robot is */
int slope_sign = 0;
int slope = 0;

int get_gradient_position_error(void)
{
	#define POSITION_VIRTUAL_LINE (600)
	#define ERROR_GAIN 						(2500/125)
	#define LINE_ERROR_POS_MAX		(+2600)
	#define LINE_ERROR_NEG_MAX    (-LINE_ERROR_POS_MAX)

	#define PIANO_BLACK_TH 		500
	#define PIANO_WHITE_TH 		100
	#define SLOPE_ABS_TH			150
	#define MIDTRACK_WHITE_TH 100

	uint8_t j = 0;
	int32_t midpoint_sum = 0;
	uint8_t midpoint_counter = 0;
	int32_t error = 0;

	static int32_t error_old = 0;

  /* Gradient track schema:

                |\    /|
  track_1 ----> | \  / | <---- track_2
                |  \/  |
	piano_1 --> __|      |__ <-- piano_2

	Blancos: 			614  	623  	527  	613  	622  	538
  Negros: 			791  	799  	739  	787  	795  	750
	Negro-Blanco: 177 	176 	212 	174 	173 	212
 */

#if 0
	/* Calculate the gradient middle point position */
	negros = 0;
	midpoint_counter = 0;
	for(i = 0; i < S/2; i++)
	{
		j = S-1-i;

		if(ADCresult[i] < PIANO_WHITE_TH || ADCresult[j] < PIANO_WHITE_TH) {
			//ADCresult[i] = ADCresult[j] = 0;
			continue;
		}
		else {
			/* Average of middle points */
			midpoint[midpoint_counter] = (int32_t)(ADCresult[i]+ADCresult[j]) >> 1
			midpoint_sum += midpoint[midpoint_counter];
			midpoint_counter ++;
			negros += 2;
		}
	}

	/* The modpoint of calculated position indicates the absolute position inside
		 the virtual line, the slope sign indicates the virtual line where is
		 the robot (track 1 or 2).
	*/
	if(midpoint_counter)
		position = midpoint_sum/midpoint_counter;


	/* Calculate the slope of the gradient */
	/* XXX: Right track --> (S7-S0) > 0 --> slope_sign > 0 */
	if(ADCresult[S] > PIANO_WHITE_TH && ADCresult[0] > PIANO_WHITE_TH &&) {
		slope[slope_counter++] = (int)ADCresult[S]-(int)ADCresult[0];

	if (robot_on_right_piano())

	if (abs(slope) > SLOPE_ABS_TH)



	/* Piano left zone */
	if(ADCresult[0] < PIANO_WHITE_TH && ADCresult[S-1] > PIANO_BLACK_TH)
		slope_sign = (int)ADCresult[S-1]-(int)ADCresult[0];

	/* Piano right zone */
	else if(ADCresult[0] > PIANO_BLACK_TH && ADCresult[S-1] < PIANO_WHITE_TH)
		slope_sign = (int)ADCresult[0]-(int)ADCresult[S-1];

	/* Gradient zone if:
	 	1. No sensor on pianos
		2. No sensor on middle white line between tracks
		3. Enough diference between first and last sensor
	*/
	else if (negros == S &&
					abs((int)ADCresult[S-1]-(int)ADCresult[0]) > SLOPE_ABS_TH
#if 1
					&&
					ADCresult[0] > (MIDTRACK_WHITE_TH) &&
					ADCresult[1] > (MIDTRACK_WHITE_TH) &&
					ADCresult[2] > (MIDTRACK_WHITE_TH) &&
					ADCresult[3] > (MIDTRACK_WHITE_TH) &&
					ADCresult[4] > (MIDTRACK_WHITE_TH) &&
					ADCresult[5] > (MIDTRACK_WHITE_TH)
					#endif
				)

	{
			slope_sign = (int)ADCresult[S-1]-(int)ADCresult[0];
	}
	//else {
		/* XXX: No slope info zone, return last valid error */
	//	return error_old;
	//}



	/* Calculate the position error of the virtual line */
	/* XXX: ERROR_GAIN set the sensibility */
	if (robot_is_between_2tracks())
		error = error_old;
	else {
		error = (POSITION_VIRTUAL_LINE - position)
		error *= slope_sign;
		error *= ERROR_GAIN;
	}

 /* Set error to constant value if we lost the track dependint on last turn
 		direcction. */
 if(negros == 0) {
	 if(giro == IZQ)
		 error = LINE_ERROR_NEG_MAX;
	 else // giro == DER
		 error = LINE_ERROR_POS_MAX;
 }
 else {
	 /* Emulate virtual line.
 		 Saturate error to the range of the former line error calculation */
	if (error < LINE_ERROR_NEG_MAX) {
		error = LINE_ERROR_NEG_MAX;
		negros = 0;
	}
	else if (error > LINE_ERROR_POS_MAX) {
		error = LINE_ERROR_POS_MAX;
		negros = 0;
	}
}
#endif
	error_old = error;
	return error;
}

void update_line_type(void)
{
	if(lin == DESCONOCIDO)
	{
	#ifdef CIRCUITO_3_LINEAS
		if(proportional > -750 && proportional < 750)
			lin = CENTRO;
		else if(proportional < 0)
			lin = IZQ;
		else
			lin = DER;
	#else
		if(proportional < 0)
			lin = IZQ;
		else
			lin = DER;
	#endif
	}

	if(negros != 0)
	{
	#ifndef CIRCUITO_3_LINEAS
		if(proportional < (float)(-(S/2 + 1)*1000/2) && lin == DER && giro == DER)
			lin = IZQ;
		else if(proportional > (float)(S/2 + 1)*1000/2 && lin == IZQ && giro == IZQ)
			lin = DER;
	#else
		if(proportional < (float)(-(S/2 + 1)*1000/2) && lin == DER && giro == DER)
			lin = CENTRO;
		else if(proportional > (float)(S/2 + 1)*1000/2 && lin == IZQ && giro == IZQ)
			lin = CENTRO;
		else if(proportional > (float)(S/2 + 1)*1000/2 && lin == CENTRO && giro == IZQ)
			lin = DER;
		else if(proportional < (float)(-(S/2 + 1)*1000/2) && lin == CENTRO && giro == DER)
			lin = IZQ;
	#endif

		if(proportional < 0)//(float)(-(S/2 + 1)*1000/2))
		{
			giro_ant = giro;
			giro = IZQ;
		}
		else //if(proportional > (float)(S/2 + 1)*1000/2)
		{
			giro_ant = giro;
			giro = DER;
		}
	}
}

void update_gradient_line_type(void)
{
	if(negros != 0)
	{
	#ifdef CIRCUITO_3_LINEAS
		#error "Circuit of 3 tracks not supported by gradient function"
	#else
		/* XXX: Right track --> (S7-S0) > 0 --> slope_sign > 0 */
		if(slope_sign > 0)
			lin = DER;
		else
			lin = IZQ;
	#endif
	}
}

void update_tracking_line(void)
{
	#ifndef CIRCUITO_3_LINEAS
			if(proportional < 0 && lin == DER && giro_ant == DER)
				lin = IZQ;
			else if(proportional > 0 && lin == IZQ && giro_ant == IZQ)
				lin = DER;
	#else
			if(proportional < 0 && lin == DER && giro_ant == DER)
				lin = CENTRO;
			else if(proportional > 0 && lin == IZQ && giro_ant == IZQ)
				lin = CENTRO;
			else if(proportional > 0 && lin == CENTRO && giro_ant == IZQ)
				lin = DER;
			else if(proportional < 0 && lin == CENTRO && giro_ant == DER)
				lin = IZQ;
	#endif
}

void update_gradient_tracking_line(void)
{
		/* TODO */
}

void do_pid(void)
{
	/* Line error */
	//proportional = get_line_position_error();
	proportional = get_gradient_position_error();

	/* Update line type (IZQ, DER, CENTRO) */
	//update_line_type();
	update_gradient_line_type();


	integral = integral + proportional;
	if(abs(integral) > integral_max)
	{
		if(integral > 0)
			integral = integral_max;
		else
			integral = -integral_max;
	}

	derivative = proportional - last_proportional;
	last_proportional = proportional;

	control_value = (float)(proportional * Kp + integral * Ki + derivative * Kd);

	if (debug_print_counter == 0 && debug_pid) {
		//debug_print_counter = sprintf (debug_print_buff,"p = %d i = %d d = %d\r\n",
		//													(int)proportional, (int)integral, (int)derivative);
		debug_print_counter = sprintf (debug_print_buff,"p = %+5d d = %+5d\r\n",
															(int)proportional, (int)derivative);
	}
}

void set_motors(void)
{
	static float velocidad = 0;

/* TODO: test higher values */
#define ACC_POSITIVA	0.5
#define ACC_NEGATIVA	2

	float v_izq = 0, v_der = 0;

	/* limite de aceleracion */
	/* XXX, podria darse el caso de que no arracase el motor al acelerar poco a poco */
	if(velocidad < v_motores)
	{
		velocidad += ACC_POSITIVA;

		if(velocidad > v_motores)
			velocidad = v_motores;
	}
	else if (velocidad > v_motores)
	{
		velocidad -= ACC_NEGATIVA;

		/* saturador de velocidad cero */
		if (velocidad < 0.0)
			velocidad = 0.0;

		if(velocidad < v_motores)
			velocidad = v_motores;
	}

	switch(ESTADO_RASTREO)
	{
		case Rastreo_cambio_lin:

			if(lin == IZQ)
				control_value = 1000;//500;
			else if(lin == DER)
				control_value = -1000;//-500;

#ifdef CIRCUITO_3_LINEAS
			else if (LINEA_CARRIL_EXTERIOR == DER)
			{
				if (!dist_izq)
					control_value = -1000;
				else
					control_value = 1000;
			}
			else
			{
				if (!dist_der)
					control_value = 1000;
				else
					control_value = -1000;
			}
#endif

			if(negros == 0)
				cont_cambio_lin += PER_PERIOD;

			break;
//		case Rastreo_freno:
//			cont_cambio_lin += PER_PERIOD;
//
//			break;
		case Rastreo_avanzar:
			control_value = 0;

			break;
		case Rastreo_normal:
			break;
	}

//	if(ESTADO_RASTREO == Rastreo_freno)
//	{
//		if(control_value > max_difference)
//			control_value = max_difference;
//		else if(control_value < -max_difference)
//			control_value = -max_difference;
//
//		v_izq = (float)(max_difference - control_value)/(max_difference) * velocidad;
//		v_der = (float)(max_difference + control_value)/(max_difference) * velocidad;
//
//		if(v_izq > 100)
//			v_izq = 100;
//		else if(v_izq < 0)
//			v_izq = 0;
//		if(v_der > 100)
//			v_der = 100;
//		else if(v_der < 0)
//			v_der = 0;
//
//		MOTOR_SPEED_set(MOTOR_IZQ, v_izq, LOCKED);
//		MOTOR_SPEED_set(MOTOR_DER, v_der, LOCKED);
//	}
//	else
//	{
		if(control_value > max_difference)
			control_value = max_difference;
		else if(control_value < -max_difference)
			control_value = -max_difference;

		if(control_value < -max_difference/2)
		{
			v_izq = (float)(-max_difference/2 - control_value)/(max_difference/2) * velocidad;
			v_der = (float)(max_difference/2 - control_value)/(max_difference/2) * velocidad;

			if(v_izq > 100)
				v_izq = 100;
			else if(v_izq < 0)
				v_izq = 0;
			if(v_der > 100)
				v_der = 100;
			else if(v_der < 0)
				v_der = 0;

			MOTOR_SPEED_set(MOTOR_IZQ, v_izq, LOCKED);
			MOTOR_SPEED_set(MOTOR_DER, v_der, FORWARD);
		}
		else if(control_value > max_difference/2)
		{
			v_izq = (float)(max_difference/2 + control_value)/(max_difference/2) * velocidad;
			v_der = (float)(-max_difference/2 + control_value)/(max_difference/2) * velocidad;

			if(v_izq > 100)
				v_izq = 100;
			else if(v_izq < 0)
				v_izq = 0;
			if(v_der > 100)
				v_der = 100;
			else if(v_der < 0)
				v_der = 0;

			MOTOR_SPEED_set(MOTOR_IZQ, v_izq, FORWARD);
			MOTOR_SPEED_set(MOTOR_DER, v_der, LOCKED);
		}
		else
		{
			v_izq = (float)(max_difference/2 + control_value)/(max_difference/2) * velocidad;
			v_der = (float)(max_difference/2 - control_value)/(max_difference/2) * velocidad;

			if(v_izq > 100)
				v_izq = 100;
			else if(v_izq < 0)
				v_izq = 0;
			if(v_der > 100)
				v_der = 100;
			else if(v_der < 0)
				v_der = 0;

			MOTOR_SPEED_set(MOTOR_IZQ, v_izq, FORWARD);
			MOTOR_SPEED_set(MOTOR_DER, v_der, FORWARD);
		}
//	}

	PWMLER = (1<<0)|(1<<MI_PWM)|(1<<MD_PWM);
}

#define RECTA		0
#define CURVA_IZQ	1
#define CURVA_DER	2

unsigned long encoder_izq_cont = 0, encoder_izq_cont_ant = 0, encoder_der_cont = 0, encoder_der_cont_ant = 0;
unsigned long encoder_izq_vel = 0, encoder_der_vel = 0;
long encoder_dif_vel = 0;
long encoder_dist = 0, encoder_dist_ant = 0;
unsigned long encoder_timer_izq = 0, encoder_timer_der = 0;
unsigned long time_pulso_izq = 0, time_pulso_der = 0;
int cont_recta = 0, cont_curva = 0;
int tramo = RECTA, tramo_ant = RECTA;

void reset_encoders(void)
{
	encoder_izq_cont = 0;
	encoder_der_cont = 0;
	encoder_izq_cont_ant = 0;
	encoder_der_cont_ant = 0;

	encoder_izq_vel = 0;
	encoder_der_vel = 0;

	encoder_timer_izq = 0;
	encoder_timer_der = 0;
}

void get_vel_encoders(void)
{
	//tomar velocidad de cada encoder
	//calcular velocidades lineal y angular reales
}

void get_pos_encoders(void)
{
	//tomar distancia de cada encoder contando los pulsos
	//calcular distancia recorrida, posici�n y orientaci�n en funci�n de la velocidad en un periodo del timer
}

unsigned int get_distance (void) {

	return (encoder_izq_cont + encoder_der_cont)/2;

}

void deteccion_curvas_rectas (void)
{
#define RECTA_TH 6
#define CURVA_TH 9
#define CONTADOR_RECTA_VALIDA 8 //6	// Cosmobot 8
#define CONTADOR_CURVA_VALIDA 4 //6 // Cosmobot 4


	if((encoder_izq_cont - encoder_izq_cont_ant) > 40 || (encoder_der_cont - encoder_der_cont_ant) > 40)
	{
		encoder_izq_vel = encoder_izq_cont - encoder_izq_cont_ant;
		encoder_der_vel = encoder_der_cont - encoder_der_cont_ant;
		encoder_dif_vel = encoder_izq_vel - encoder_der_vel;

		encoder_izq_cont_ant = encoder_izq_cont;
		encoder_der_cont_ant = encoder_der_cont;

		if(abs(encoder_dif_vel) <= RECTA_TH && tramo != RECTA) {
			cont_curva = 0;
			cont_recta++;
		}
		else
			cont_recta = 0;

		if(abs(encoder_dif_vel) >= CURVA_TH)
		{
            /* XXX: posible bug al imponer condicion de tramo ???
               Creo que no, as� se limita el valor del contador y no se desborda */
			if(encoder_dif_vel >= CURVA_TH && tramo != CURVA_DER)
				cont_curva++;
			else if (tramo != CURVA_IZQ)
				cont_curva--;
		}
		else
			cont_curva = 0;

		if(cont_recta >= CONTADOR_RECTA_VALIDA && tramo != RECTA)
		{
			tramo = RECTA;
			encoder_dist_ant = encoder_dist;
			encoder_dist = (encoder_izq_cont + encoder_der_cont)/2;
			reset_encoders();
			cont_curva = 0;

		}
		else if(cont_curva <= -CONTADOR_CURVA_VALIDA && tramo != CURVA_IZQ)
		{
			tramo = CURVA_IZQ;
			encoder_dist_ant = encoder_dist;
			encoder_dist = (encoder_izq_cont + encoder_der_cont)/2;
			reset_encoders();
			cont_recta = 0;
		}
		else if(cont_curva >= CONTADOR_CURVA_VALIDA && tramo != CURVA_DER)
		{
			tramo = CURVA_DER;
			encoder_dist_ant = encoder_dist;
			encoder_dist = (encoder_izq_cont + encoder_der_cont)/2;
			reset_encoders();
			cont_recta = 0;
		}
	}


	if(tramo == RECTA)
	{
		LED_set(LED_1, OFF);
		LED_set(LED_2, ON);
	}
	else
	{
		LED_set(LED_1, ON);
		LED_set(LED_2, OFF);
	}
}

int deteccion_curvas_rectas2 (void)
{
#define PROPORTIONAL_CURVA_IZQ_TH	-200
#define PROPORTINAL_CURVA_DER_TH	+200
#define CONTADOR_CURVA_IZQ_TH			1
#define CONTADOR_CURVA_DER_TH			1
#define CONTADOR_RECTA_TH					5

int ret = 0;

/* v= 1000mm/s
 	 T_pid = 1ms
	 t = v * T_pid*contador
	 t = 1000 * 1ms*10 = 10mm
*/

	static int contador_curva_izq = 0;
	static int contador_curva_der = 0;
	static int contador_recta = 0;
	static int i=0;
	static int tramo_old = RECTA;

	if (proportional < PROPORTIONAL_CURVA_IZQ_TH) {
		contador_curva_izq++;
		contador_curva_der = 0;
		contador_recta = 0;

		// Frenada despues de recta
		if (tramo != CURVA_IZQ && tramo_old == RECTA)
			ret = 1;

		if (contador_curva_izq > CONTADOR_CURVA_IZQ_TH && tramo != CURVA_IZQ) {
			tramo = CURVA_IZQ;
			i++;
			reset_encoders();
			//if (debug_print_counter == 0) {
			//	debug_print_counter = sprintf (debug_print_buff,"%4d CURVA IZQ    \n\r", i);
			//}
		}
	}
	else if (proportional > PROPORTINAL_CURVA_DER_TH) {
		contador_curva_izq = 0;
		contador_curva_der++;
		contador_recta = 0;

		// Frenada despues de recta
		if (tramo != CURVA_DER && tramo_old == RECTA)
			ret = 1;

		if (contador_curva_der > CONTADOR_CURVA_DER_TH && tramo != CURVA_DER) {
			tramo = CURVA_DER;
			i++;
			reset_encoders();
			//if (debug_print_counter == 0) {
			//	debug_print_counter = sprintf (debug_print_buff,"%4d CURVA DER    \n\r", i);
			//}
		}
	}
	else {
		contador_curva_izq = 0;
		contador_curva_der = 0;
		contador_recta ++;

		if (contador_recta > CONTADOR_RECTA_TH && tramo != RECTA) {
			tramo = RECTA;
			i++;
			reset_encoders();
			//if (debug_print_counter == 0) {
			//	debug_print_counter = sprintf (debug_print_buff,"%4d RECTA        \n\r", i);
			//}
		}
	}


	if(tramo == CURVA_IZQ) {
		LED_set(LED_1, ON);
		LED_set(LED_2, OFF);
	}
	else if(tramo == CURVA_DER) {
		LED_set(LED_1, OFF);
		LED_set(LED_2, ON);
	}
	else {
		LED_set(LED_1, ON);
		LED_set(LED_2, ON);
	}

	if(tramo != tramo_old) {
		tramo_old = tramo;
	}

	return ret;
}


/*****************************************
  PASOS PARA AJUSTAR PROGRAMA

  DETECCION DE CURVAS Y RECTAS
  ----------------------------

  	1. Rectas m�s sensibles --> disminuir CONTADOR_RECTA_VALIDA
	2. Curvas m�s sensible  --> disminuir CONTADOR_CURVA_VALIDA


  SINCRONIZACION CIRCUITO
  -----------------------

  	1. Se detecta la "recta-curva larga" cuando se detecta dos curvas diferentes CONSECUTIVAS
	   (IZQ-DER o DER-IZQ).
	2. Se deja de detectar la "recta-curva larga" cuando despues de una recta viene una curva a IZQ.


  ACELERACION EN RECTA
  ---------------------
  	1. Solo se acelera en la "recta-curva larga"
  	2. Distancia de inicio de aceleracion --> ACELERACION_DIST_RECTA_MIN
	3. Distancia de fin de aceleracion    --> ACELERACION_DIST_RECTA_MAX
	4. Se fija la velocidad maxima dependiendo del tramo -> v_max.


  SEGUIMIENTO OPONENTE
  ---------------------
	1. Modifica la velocidad de los motores --> v_motores.
  	2. Se disminuye v_motores hasta 0 "si el sensor > CS_DIST_OPP_TH_HI"
	3. Se aumenta v_motores hasta la velocidad m�xima --> v_max, si "si el sensor < CS_DIST_OPP_TH_LOW"


  ADELANTAMIENTOS
  ---------------

	Condiciones de adelantamiento:

	1. Se ha detectado un robot m�s lento --> flag do_adelantamiento
	2. Estamos en una recta y es la "recta-curva larga"
	3. La distancia recorrida es mayor que ADELANTAMIENTO_DIST_RECTA_MIN
	4. La distancia recorrida es menor que ADELANTAMIENTO_DIST_RECTA_MAX
	5. No se detectan obstaculos en los laterales.


	Ajustes trayectoria adelantamiento:

	1. Velocidad de la secuencia de adelantamiento --> ADELANTAMIENTO_VELOCIDAD
	2. Tiempo de giro a la otra linea 			   --> ADELANTAMIENTO_TIEMPO_GIRO

	Ajustes detecci�n robot m�s lento:

	1. Se detecta robot si el sensor de distancia es mayor que --> ADELANTAMIENTO_TH
	   y durante varias veces                                  --> ADELANTAMIENTO_TH_CUENTAS


 ****************************************/

char do_adelantamiento = 0;
char adelantamiento_reciente = 0;

char recta_siguiente_es_larga = 0;
char tramo_anterior = RECTA;

int contador_rectas = 0, contador_curvas_der = 0, contador_curvas_izq = 0, contador_curvas_alternas = 0;
int contador_vueltas_abs = 0, contador_vueltas_rel = 0;

char do_seguimiento_rebufo = 0;
float rebufo_err_i = 0.0;
float rebufo_err_p = 0.0, rebufo_err_p_old = 0.0, rebufo_err_d = 0.0;



/* dimensiones y constantes geometricas */
#define RUEDA_D_25_5 	(25.5)
#define RUEDA_D_27_5 	(27.5)
#define RUEDA_D			(RUEDA_D_25_5)
#define RUEDA_L			(RUEDA_D * 3.1416)
#define PULSOS_REV 		(5*2*10)
#define PULSOS_MM		(PULSOS_REV/RUEDA_L) /* 1.2483 pulsos/mm @RUEDA_D_25_5 */
														/* 1.1575 pulsos/mm @RUEDA_D_25_5 */
#define MM_PULSOS		(RUEDA_L/PULSOS_REV)


/* adelantamientos */
#define ADELANTAMIENTO_VELOCIDAD 		30
#define ADELANTAMIENTO_TIEMPO_GIRO 		0.025 //0.02
#define ADELANTAMIENTO_TH				200
#define ADELANTAMIENTO_TH_CUENTAS		10

/* seguimiento todo o nada */
#define CS_DIST_OPP_TH_HI 			200
#define CS_DIST_OPP_TH_LOW			200	 //180
#define CS_DIST_OPP_VEL_INC		0.5
#define CS_DIST_OPP_VEL_DEC		0.5//0.1

/* seguimiento rebufo */
#define REBUFO_TH_ON    100
#define REBUFO_TH_OFF   0
#define REBUFO_CONSIGN  ((float)(ADELANTAMIENTO_TH-20))

#define REBUFO_KP   0.3
#define REBUFO_KI   0.0
#define REBUFO_KD   1.25

#define REBUFO_ERROR_I_MAX  50



/*****************************************
  ELEGIR AQUI EL MODO DE COMPETICION
  ****************************************/

//#define MODO_PERSECUCION
#define MODO_CARRERAS

#if defined(MODO_PERSECUCION)
	//#define DO_SYNC_GLOBAL_ROBOT_EXPO_2016
	//#define DO_DIFERENTE_VELOCIDAD_POR_TRAMOS
	#define DO_DIFERENTE_VELOCIDAD_CURVA_RECTA

#elif defined(MODO_CARRERAS)
	//#define DO_SYNC_RECTA_LARGA
	//#define DO_SYNC_GLOBAL_ROBOT_EXPO_2016
	//#define DO_DIFERENTE_VELOCIDAD_CURVA_RECTA
	#define DO_ADELANTAMIENTOS
	//#define DO_SEGUIMIENTO_OPONENTE_TODO_NADA
	#define DO_SEGUIMIENTO_OPONENTE_REBUFO
	//#define DO_CAMBIO_CARRIL_INTERIOR
	//#define DO_ADELANTAMIENTOS_HACK

#else	/* modo no definido */
#error No se ha definido un modo de competicion

#endif	/* MODO_PERSECUCION, MODO_CARRERAS */

/* XXX: HACK */
#ifdef DO_ADELANTAMIENTOS_HACK
#warning XXX DO_ADELANTAMIENTOS_HACK
#endif

/* XXX: parametros pista de pruebas */
//#define PISTA_PRUEBAS
#ifdef PISTA_PRUEBAS

	#define ACELERACION_DIST_RECTA_MIN		(10*PULSOS_MM)
	#define ACELERACION_DIST_RECTA_MAX		(500*PULSOS_MM)

	#define ADELANTAMIENTO_DIST_RECTA_MIN	(10*PULSOS_MM)
	#define ADELANTAMIENTO_DIST_RECTA_MAX	(500*PULSOS_MM)

	#define CAMBIO_CARRIL_INTERIOR_DIST_RECTA_MIN (10*PULSOS_MM)
	#define CAMBIO_CARRIL_INTERIOR_DIST_RECTA_MAX (500*PULSOS_MM)
	#define CAMBIO_CARRIL_INTERIOR_VUELTAS_MIN	  (2)

#else /* !PISTA_PRUEBAS */

	/* XXX TODO */

	#define ACELERACION_DIST_RECTA_MIN		(10*PULSOS_MM)
	#define ACELERACION_DIST_RECTA_MAX		(2000*PULSOS_MM)

	#define ADELANTAMIENTO_DIST_RECTA_MIN	(700*PULSOS_MM)
	#define ADELANTAMIENTO_DIST_RECTA_MAX	(1000*PULSOS_MM)

	#define CAMBIO_CARRIL_INTERIOR_DIST_RECTA_MIN (10*PULSOS_MM)
	#define CAMBIO_CARRIL_INTERIOR_DIST_RECTA_MAX (1000)

	#define CAMBIO_CARRIL_INTERIOR_VUELTAS_MIN	 (2)

#endif

void circuito_init_a(void)
{
	/* contador de vueltas */
	contador_vueltas_rel = 0;
	contador_vueltas_abs = 0;

	/* reset contador rectas y curvas */
	contador_rectas = 1;
	contador_curvas_der = 0;
	contador_curvas_izq = 0;
	contador_curvas_alternas = 0;

	/* tramo actual */
	tramo = tramo_anterior = RECTA;
}

void circuito_init_b(void)
{
	/* contador de vueltas */
	contador_vueltas_rel = 0;
	contador_vueltas_abs = 0;

	/* reset contador rectas y curvas */
	contador_rectas = 3;
	contador_curvas_der = 0;
	contador_curvas_izq = 2;
	contador_curvas_alternas = 0;

	/* tramo actual */
	tramo = tramo_anterior = RECTA;
}

void loop_PID(void)
{
	int longitud = 0;

/* descomentar para sacar mensajes de depuracion */
//#define __DEBUG__

	static float v_max = 0.0;

	do_pid();


#ifdef DO_ADELANTAMIENTOS
	if(ESTADO_RASTREO == Rastreo_normal)
	{
#endif

		longitud = get_distance();

		/* deteccion de curvas y rectas */
		//deteccion_curvas_rectas ();
		deteccion_curvas_rectas2 ();
		//if (deteccion_curvas_rectas2 ()) {
		//	v_motores = 0;
		//	goto end;
		//}


	    /* estadisticas circuito */
	    if (tramo != tramo_anterior)
        {
#ifdef __DEBUG__
				debug_print_counter = sprintf (debug_print_buff,
				"   %s %02d, l %05d\r\n",
				((tramo_anterior == RECTA)? "recta":
				 (tramo_anterior == CURVA_IZQ)? "curva_izq":"curva_der"),
				((tramo_anterior == RECTA)? contador_rectas:
				 (tramo_anterior == CURVA_IZQ)? contador_curvas_izq:
				 contador_curvas_der), (int)(longitud*MM_PULSOS));
#endif
            /* contador de rectas */
            if (tramo == RECTA)
                contador_rectas ++;

            /* contador de curvas IZQ */
            else if (tramo == CURVA_IZQ)
                contador_curvas_izq ++;

            /* contador de curvas DER */
            else if (tramo == CURVA_DER)
                contador_curvas_der ++;

#if 0
            /* XXX contador de curvas alternas */
            if ((tramo==CURVA_DER) || (tramo==CURVA_IZQ))
                contador_curvas_alternas ++;
            else
                contador_curvas_alternas = 0;
#endif

/* sincronizaci�n de vueltas para la Global Robot Expo 2016 */
#ifdef DO_SYNC_GLOBAL_ROBOT_EXPO_2016

            /* detecci�n de inicio de circuito */
#ifdef PISTA_PRUEBAS
            if (contador_rectas > 3 && tramo == RECTA)
#else
            if (contador_rectas > 3 && contador_curvas_der>=1 && tramo == RECTA)
#endif
            {
				/* contador de vueltas */
                contador_vueltas_rel ++;
                contador_vueltas_abs ++;

				#ifdef __DEBUG__
				debug_print_counter =
				sprintf(debug_print_buff,
				"nueva vuelta: v %02d (rel %02d), r %02d, izq %02d, der %02d\n\r",
				contador_vueltas_abs, contador_vueltas_rel,
				contador_rectas, contador_curvas_izq, contador_curvas_der);
				#endif

				/* reset contador rectas y curvas */
				contador_rectas = 1;
                contador_curvas_der = 0;
                contador_curvas_izq = 0;
				contador_curvas_alternas = 0;

            }
#endif /* DO_SYNC_GLOBAL_ROBOT_EXPO_2016 */

			/* guarda info */
			tramo_anterior = tramo;
		}


/* XXX: definir para adaptar la velocidad a cada tramo */
#if defined(DO_DIFERENTE_VELOCIDAD_POR_TRAMOS)

		/* TODO: velocidade de recta */
		#define VELOCIDAD_RECTA_RAPIDA 	80
		#define VELOCIDAD_RECTA_LENTA 	60

		/* TODO: velocidades de curva */
		#define VELOCIDAD_CURVA_RAPIDA 	60
		#define VELOCIDAD_CURVA_LENTA 	60


		/* parser de tramos rectos */
		if (tramo == RECTA)
		{
			/* longitudes de recta */

			/* tramo en U */
			#define R1 4164
			#define R2 1740
			#define R3 1664

			#define OFFSET_DETECCION_RECTA	600
			#define LONGITUD_RECTA_1 			((R1-OFFSET_DETECCION_RECTA)*PULSOS_MM)
			#define LONGITUD_RECTA_2 			((R2-OFFSET_DETECCION_RECTA)*PULSOS_MM)
			#define LONGITUD_RECTA_3 			((R3-OFFSET_DETECCION_RECTA)*PULSOS_MM)

			switch (contador_rectas)
			{
				/*
					Estrategia
					----------

					Al entrar en la recta aceleramos, al salir deceleramos
					para entrar en el siguiente tramo (curva) sin derrapar.

				*/

				case 1:
					if (get_distance() < (long int)LONGITUD_RECTA_1) {
						v_recta = VELOCIDAD_RECTA_RAPIDA;
						v_curva = VELOCIDAD_CURVA_LENTA;
					}
					else {
						v_recta = VELOCIDAD_RECTA_LENTA;
						v_curva = VELOCIDAD_CURVA_LENTA;
					}
					break;

				case 2:
					if (get_distance() < (long int)LONGITUD_RECTA_2) {
						v_recta = VELOCIDAD_RECTA_RAPIDA;
						v_curva = VELOCIDAD_CURVA_LENTA;
					}
					else {
						v_recta = VELOCIDAD_RECTA_LENTA;
						v_curva = VELOCIDAD_CURVA_LENTA;
					}
					break;

				case 3:
					if (get_distance() < (long int)LONGITUD_RECTA_3) {
						v_recta = VELOCIDAD_RECTA_RAPIDA;
						v_curva = VELOCIDAD_CURVA_LENTA;
					}
					else {
						v_recta = VELOCIDAD_RECTA_LENTA;
						v_curva = VELOCIDAD_CURVA_LENTA;
					}
					break;

				default:
					/* caso para posibles rectas en en tramo en S */
					v_recta = VELOCIDAD_RECTA_LENTA;
					v_curva = VELOCIDAD_CURVA_LENTA;
					break;
			}
		}

		/* parser de tramos curvos a la izquierda */
		if (tramo == CURVA_IZQ)
		{
			/* longitudes de curva */

			/* tramo en U */
			#define C1 657
			#define C2 598

			/* tramo en Z */
			#define C3 154	// esta no creo que la detectemos
			#define C4 331
			#define C5 444
			#define C6 502
			#define C7 843

			/* tramo en S */
			#define C8 1732
			#define C9 5314
			#define C10 1545

			#define OFFSET_DETECCION_CURVA	300
			#define LONGITUD_CURVA_1 			((C1-OFFSET_DETECCION_CURVA)*PULSOS_MM)
			#define LONGITUD_CURVA_2 			((C2-OFFSET_DETECCION_CURVA)*PULSOS_MM)
			#define LONGITUD_CURVA_LARGA 		((C8-OFFSET_DETECCION_CURVA)*PULSOS_MM)

			switch (contador_curvas_izq)
			{
				/*
					Estrategia
					----------

					Al entrar en la curva venimos con velocidad lenta del
					final de la recta, a mitad de curva aceleramos para
					entrar en el siguiente tramo a velocidad alta.

					Si es una recta, perfecto.

					Si es una curva de sentido contrario, al entrar en la
					curva se frena.

					Caso de intersecci�n entre curvas del tramo en S.
					Dos posibles situaciones:

					1. Si se detecta recta en la intersecci�n se decelerar�.
					2. Si se detecta la siguiente curva de direcc�n contraria,
						se	decelerar� durante la curva.

					XXX - Peligro de derrape en situaci�n 2 -

				*/

				case 1:
					if (get_distance() < (long int)LONGITUD_CURVA_1) {
						v_recta = VELOCIDAD_RECTA_RAPIDA;
						v_curva = VELOCIDAD_CURVA_LENTA;
					}
					else {
						v_recta = VELOCIDAD_RECTA_RAPIDA;
						v_curva = VELOCIDAD_CURVA_LENTA;
					}
					break;

				case 2:
					if (get_distance() < (long int)LONGITUD_CURVA_2) {
						v_recta = VELOCIDAD_RECTA_RAPIDA;
						v_curva = VELOCIDAD_CURVA_LENTA;
					}
					else {
						v_recta = VELOCIDAD_RECTA_RAPIDA;
						v_curva = VELOCIDAD_CURVA_LENTA;
					}
					break;

				case 3:
				case 4:
				case 5:
				case 6:
				default:
					if (get_distance() < (long int)LONGITUD_CURVA_LARGA) {
						v_recta = VELOCIDAD_RECTA_RAPIDA;
						v_curva = VELOCIDAD_CURVA_LENTA;
					}
					else {
						v_recta = VELOCIDAD_RECTA_RAPIDA;
						v_curva = VELOCIDAD_CURVA_RAPIDA;
					}
					break;
			}
		}

		/* parser de tramos curvos a la derecha */
		if (tramo == CURVA_DER)
		{
			switch (contador_curvas_der)
			{
				case 1:
				case 2:
				case 3:
				default:
					if (get_distance() < (long int)LONGITUD_CURVA_LARGA) {
						v_recta = VELOCIDAD_RECTA_RAPIDA;
						v_curva = VELOCIDAD_CURVA_LENTA;
					}
					else {
						v_recta = VELOCIDAD_RECTA_RAPIDA;
						v_curva = VELOCIDAD_CURVA_RAPIDA;
					}
					break;
			}
		}

		/* asignaci�n velocidad maxima */
		if (tramo == RECTA) v_max = v_recta;
		else 				v_max = v_curva;

/* velocidad maxima en funci�n de curva o recta */
#elif defined(DO_DIFERENTE_VELOCIDAD_CURVA_RECTA)

#ifdef DO_ADELANTAMIENTOS
		if (tramo == RECTA && /* ADCdist < CS_DIST_OPP_TH_LOW && */
#else
		if (tramo == RECTA &&
#endif
			0
			//get_distance() > (long int)ACELERACION_DIST_RECTA_MIN &&
			//get_distance() < (long int)ACELERACION_DIST_RECTA_MAX
#ifdef DO_SYNC_RECTA_LARGA
			&& recta_siguiente_es_larga
#endif
			)
		{
			v_max = v_recta_larga;
		}
		else if (tramo == RECTA)
			v_max = v_recta;
		else  {
			v_max = v_curva;
		}
#else /* !DO_DIFERENTE_VELOCIDAD_POR_TRAMOS  && !DO_DIFERENTE_VELOCIDAD_CURVA_RECTA*/

		v_max = v_curva;

#endif /* DO_DIFERENTE_VELOCIDAD_CURVA_RECTA */


#if defined(DO_SEGUIMIENTO_OPONENTE_TODO_NADA)
		/* control de distancia oponente */
		if (ADCdist >= CS_DIST_OPP_TH_HI) {
			/* decrementar velocidad */
			if (v_motores > 0.0)
				v_motores -= CS_DIST_OPP_VEL_DEC;
			else
				v_motores = 0;
		}
		else if (ADCdist < CS_DIST_OPP_TH_LOW) {
			/* incrementar velocidad */
			if (v_motores < v_max)
				v_motores += CS_DIST_OPP_VEL_INC;
			else
				v_motores = v_max;
		}
#elif defined(DO_SEGUIMIENTO_OPONENTE_REBUFO)
#warning DO_SEGUIMIENTO_OPONENTE_REBUFO
		/* trigger de seguimeinto por rebufo */
		if (ADCdist > REBUFO_TH_ON && !do_seguimiento_rebufo) {
            do_seguimiento_rebufo = 1;
        }
		//else if (ADCdist < REBUFO_TH_OFF && do_seguimiento_rebufo) {
        //    do_seguimiento_rebufo = 0;
        //}

        /* control pid */
        if (do_seguimiento_rebufo)
        {
            /* error proporcional */
            rebufo_err_p =  ((float)REBUFO_CONSIGN - (float)ADCdist);

            /* error integral */
            rebufo_err_i += rebufo_err_p;
            if (abs(rebufo_err_i) > REBUFO_ERROR_I_MAX) {
                rebufo_err_i = (rebufo_err_i > 0? REBUFO_ERROR_I_MAX:-REBUFO_ERROR_I_MAX);
            }

            /* error derivativo */
            rebufo_err_d = (rebufo_err_p_old - rebufo_err_p);
            rebufo_err_p_old = rebufo_err_p;

            /* calculo pid */
            v_motores = v_max;
						v_motores += (rebufo_err_p * REBUFO_KP);
            v_motores += (rebufo_err_i * REBUFO_KI);
            v_motores += (rebufo_err_d * REBUFO_KD);

            /* saturador */
            if (v_motores > v_max)
                v_motores = v_max;
            else if (v_motores < 0)
                v_motores = 0;

        }
        else {
            rebufo_err_p = rebufo_err_p_old = rebufo_err_i = rebufo_err_d = 0;
            v_motores = v_max;
        }
#else  /* !DO_SEGUIMIENTO_OPONENTE_TODO_NADA & !DO_SEGUIMIENTO_OPONENTE_REBUFO */
		v_motores = v_max;
#endif /* DO_SEGUIMIENTO_OPONENTE_TODO_NADA else DO_SEGUIMIENTO_OPONENTE_REBUFO */


#ifdef DO_ADELANTAMIENTOS
		/* trigger adelantamiento */
		if (ADCdist > ADELANTAMIENTO_TH && tramo == RECTA)
		{
			cont_dist++;
			if(cont_dist >= ADELANTAMIENTO_TH_CUENTAS)
			{
				cont_dist = ADELANTAMIENTO_TH_CUENTAS;
				do_adelantamiento = 1;
#ifdef DO_ADELANTAMIENTOS_HACK
				do_adelantamiento = 0;
#endif
			}
		}


		/* hay que adelantar y RECTA m�s larga y tramo de adelantamiento y no oponentes a los lados */
		if (do_adelantamiento &&  (contador_rectas > 2) &&
			tramo == RECTA &&
#ifdef DO_SYNC_RECTA_LARGA
			recta_siguiente_es_larga &&
#endif
			get_distance() < (long int)ADELANTAMIENTO_DIST_RECTA_MAX &&
			get_distance() > (long int)ADELANTAMIENTO_DIST_RECTA_MIN &&
#ifndef CIRCUITO_3_LINEAS
			((lin == IZQ && !dist_der) || (lin == DER && !dist_izq))
#else
			((lin == IZQ && !dist_der) || (lin == DER && !dist_izq) ||
			 (lin == CENTRO && (!dist_izq || !dist_der)))
#endif
			/* && !adelantamiento_reciente*/)
		{
			ESTADO_RASTREO = Rastreo_cambio_lin;
			v_motores = ADELANTAMIENTO_VELOCIDAD; /* XXX, infuye en la forma de adelantar */
			cont_cambio_lin = 0;
			cont_dist = 0;
			do_adelantamiento = 0;
			do_seguimiento_rebufo = 0;
			adelantamiento_reciente = 1;
			contador_vueltas_rel = 0;
		}
#ifdef DO_CAMBIO_CARRIL_INTERIOR
		/* estamos en carril exterior y RECTA y tramo de cambio de carril y no oponentes a los lados */
        else if (lin == LINEA_CARRIL_EXTERIOR &&
            	tramo == RECTA &&
				get_distance() < (long int)CAMBIO_CARRIL_INTERIOR_DIST_RECTA_MAX &&
				get_distance() > (long int)CAMBIO_CARRIL_INTERIOR_DIST_RECTA_MIN &&
				!dist_der && !dist_izq &&
            	!adelantamiento_reciente &&
				contador_vueltas_rel > CAMBIO_CARRIL_INTERIOR_VUELTAS_MIN)
        {
			ESTADO_RASTREO = Rastreo_cambio_lin;
			v_motores = ADELANTAMIENTO_VELOCIDAD; /* XXX, infuye en la forma de adelantar */
			cont_cambio_lin = 0;
			cont_dist = 0;
			do_seguimiento_rebufo = 0;
			contador_vueltas_rel = 0;
        }

#endif
        /* reset flag de adelantamiento reciente */
        if (adelantamiento_reciente && (tramo != RECTA))
            adelantamiento_reciente = 0;
	}
//	else if(ESTADO_RASTREO == Rastreo_freno && cont_cambio_lin > 0.1)
//	{
//		ESTADO_RASTREO = Rastreo_cambio_lin;
//		if(velocidad > 80)
//			velocidad = 80;
//		cont_cambio_lin = 0;
//	}
	else if(ESTADO_RASTREO == Rastreo_cambio_lin && cont_cambio_lin > ADELANTAMIENTO_TIEMPO_GIRO)	/* XXX */
		ESTADO_RASTREO = Rastreo_avanzar;
	else if(ESTADO_RASTREO == Rastreo_avanzar && negros != 0)
	{
		ESTADO_RASTREO = Rastreo_normal;

		/* XXX: velocidad maxima en funci�n de curva o recta */
		if (tramo == RECTA)
			v_max = v_recta;
		else
			v_max = v_curva;

		cont_dist = 0;

		/* Update line at the end of track change */
		//update_tracking_line();
		//update_gradient_tracking_line();
	}
	else
		cont_dist = 0;
 #endif

 end:

	/* aplica v_motors a los motores, XXX incluye limitador de aceleracion */
	set_motors();
}
