#ifndef CONTROL_C
#define CONTROL_C

#include <lpc213x.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "LPClib.h"
#include "target.h"
#include "declaraciones.h"
#include "font.h"
#include "s1d15g00.h"
#include "PID.h"


/********************** FUNCIONES DE CONTROL **********************/
void circuito_init_a(void);
void circuito_init_b(void);

void selec_salida(void)
{
	#define SALIDA_A 	0
	#define SALIDA_B 	1
	static char posicion_salida = SALIDA_B;

	if(button_load == ON && flag == '1' && cont_sel >= 0.05)
	{
		if(posicion_salida == SALIDA_A)
		{
			posicion_salida = SALIDA_B;
			LED_set(LED_1, OFF);
			LED_set(LED_2, ON);
			circuito_init_b();
		}
		else
		{
			posicion_salida = SALIDA_A;
			LED_set(LED_1, ON);
			LED_set(LED_2, OFF);
			circuito_init_a();
		}
	}
	else if(cont_sel < 0.05 && flag == '1')
		cont_sel += PER_PERIOD;

	if(button_load == ON)
	{
		cont_sel = 0;
		flag = '0';
	}
	else // button_load == OFF
		flag = '1';
}

void selec_velocidad(void)
{
	if(button_load == ON && flag == '1' && cont_sel >= 0.05)
	{
		if(v_curva == (float)60)
		{
			LED_set(LED_1, ON);
			LED_set(LED_2, OFF);

			v_curva = 70;
		}
		else if(v_curva == (float)70)
		{
			LED_set(LED_1, OFF);
			LED_set(LED_2, ON);

			v_curva = 80;
		}
		else if(v_curva == (float)80)
		{
			LED_set(LED_1, ON);
			LED_set(LED_2, ON);

			v_curva = 90;
		}
		else // if(v_curva == 60)
		{
			LED_set(LED_1, OFF);
			LED_set(LED_2, OFF);

			v_curva = 60;
		}
//		if(v_curva == (float)40)
//		{
//			LED_set(LED_1, ON);
//			LED_set(LED_2, OFF);
//
//			v_curva = 50;
//		}
//		else if(v_curva == (float)50)
//		{
//			LED_set(LED_1, OFF);
//			LED_set(LED_2, ON);
//
//			v_curva = 60;
//		}
//		else if(v_curva == (float)60)
//		{
//			LED_set(LED_1, ON);
//			LED_set(LED_2, ON);
//
//			v_curva = 70;
//		}
//		else // if(v_curva == 70)
//		{
//			LED_set(LED_1, OFF);
//			LED_set(LED_2, OFF);
//
//			v_curva = 40;
//		}
	}
	else if(cont_sel < 0.05 && flag == '1')
		cont_sel += PER_PERIOD;

	if(button_load == ON)
	{
		cont_sel = 0;
		flag = '0';
	}
	else // button_load == OFF
		flag = '1';
}

void leer_pulsadores(void)
{
	button_load = BUTTON_status(BUTTON_LOAD);
	button_b1 = BUTTON_status(BUTTON_B1);
	button_b2 = BUTTON_status(BUTTON_B2);
}

float sensor_val[S] = 0.0;
float sensor_slope[S] = 0.0;
float sensor_gain[S] = 0.0;
float sensor_offset[S] = 0.0;
float sensor_gain_ref = 0.0;
float sensor_offset_ref = 0.0;
float sensor_slope_ref = 0.0;
uint8_t calibration_done = 0;

void leer_sensores(void)
{
	ADCresult[0] = LINE_SENSOR_read(LINE_1);
	ADCresult[1] = LINE_SENSOR_read(LINE_2);
	ADCresult[2] = LINE_SENSOR_read(LINE_3);
	ADCresult[3] = LINE_SENSOR_read(LINE_4);
	ADCresult[4] = LINE_SENSOR_read(LINE_5);
	ADCresult[5] = LINE_SENSOR_read(LINE_6);
	ADCdist = ADC_read(DIST);

	/* Gain and offset corrections */
	if (calibration_done)
	{
		for(i=0; i<S; i++) {
			/* Gain correction */
			sensor_val[i] = ADCresult[i]*sensor_gain[i];

			/* Offset correction */
			sensor_val[i] += sensor_offset[i];

			/* Back to int range */
			ADCresult[i] = (uint16_t)(sensor_val[i]);
		}
	}

	dist_izq = (DISTANCE_digital_read(DIST_IZQ) == '1');
	dist_der = (DISTANCE_digital_read(DIST_DER) == '1');
}

void estimar_umbrales(void)
{
	uint32_t val_sum = 0;
	cont = 0;

	for(i=0; i<S; i++)
	{
		if(ADCresult[i] < blanco[i])
			blanco[i] = ADCresult[i];
		if(ADCresult[i] > negro[i])
			negro[i] = ADCresult[i];

		if((negro[i]-blanco[i]) > 450)
		{
			cont++;
			umbral[i] = (negro[i]+blanco[i])/2;
		}
	}

	if(cont == S)
	{
		LED_set(LED_1, OFF);
		LED_set(LED_2, ON);

		/* Calculate calibration corrections */

		/* Offset and slope references */
		#define SENSOR_REF 2
		sensor_slope_ref = negro[SENSOR_REF]-blanco[SENSOR_REF];
		sensor_offset_ref = blanco[SENSOR_REF];

		/* Correction factors */
		for(i=0; i<S; i++)
		{
			/* Gain  correction */
			sensor_slope[i] = negro[i]-blanco[i];
			sensor_gain[i] = sensor_slope_ref/sensor_slope[i];

			/* Offset correction */
			sensor_offset[i] = sensor_offset_ref-(blanco[i]*sensor_gain[i]);
		}

		sensor_gain[SENSOR_REF] = 1.0;
		sensor_offset[SENSOR_REF] = 0.0;

		/* Set calibration flag to True*/
		calibration_done = 1;
	}

	/* Gradient stuff */
	//for(i=0; i<S; i++)
	//	val_sum += (blanco[i]+negro[i]) >> 2;
	//virtual_line_position = val_sum/S;
}

void representar_sensores(void)
{
	if(modo == DIGITAL)
	{
		for(i = 0; i < S; i++)
		{
			if(ADCresult[i] > umbral[i])
				LCDSetRect((120-(i*10)), 10, (125-(i*10)), 15, 1, RED);
			else
				LCDSetRect((120-(i*10)), 10, (125-(i*10)), 15, 1, WHITE);
		}
	}
	else
	{
		for(i = 0; i < S; i++)
		{
			sprintf(cadena, "%4d", ADCresult[i]);
			LCDPutStr(cadena, (120-(i*10)), 15, SMALL, WHITE, BLACK);
		}
	}
}

void representar_estado(void)
{
	sprintf(cadena, "EST %c", ESTADO);
	LCDPutStr(cadena, 86, 60, SMALL, WHITE, BLACK);
}

void actualizar(void)
{
	// Visualizaci�n de los datos
	if(lin == IZQ)
	{
		LED_set(LED_1, ON);
		LED_set(LED_2, OFF);
	}
	else if(lin == DER)
	{
		LED_set(LED_1, OFF);
		LED_set(LED_2, ON);
	}
	else if(lin == CENTRO)
	{
		LED_set(LED_1, ON);
		LED_set(LED_2, ON);
	}
}


#endif
