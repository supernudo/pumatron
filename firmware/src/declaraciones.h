#ifndef DECLARACIONES_H
#define DECLARACIONES_H

#include <stdbool.h>
#include <stdint.h>
#include "LPClib.h"

// Estados para implementar la m�quina de estados
#define Parado 				'0'
#define Seleccion_salida	'1'
#define Seleccion_velocidad	'2'
#define Umbralizar_sensores	'3'
#define Espera				'4'
#define Rastrear			'5'

#define Rastreo_normal		'0'
#define Rastreo_freno		'1'
#define Rastreo_cambio_lin	'2'
#define Rastreo_avanzar		'3'

#define S 6					// N�mero de sensores

#define IZQ		'1'
#define DER		'2'
#define CENTRO	'3'
#define DESCONOCIDO	'4'

/* TODO linea exterior */
#define LINEA_CARRIL_EXTERIOR   DER


#define DIGITAL		'0'
#define ANALOGICO	'1'


// Variables auxiliares
extern char ESTADO, ESTADO_RASTREO;
extern char cadena[200];
extern float start;
extern unsigned int seg_espera;
extern float cont, cont2, cont3;
extern int i, t;
extern char flag;
extern float cont_sel;
extern float cont_bat;
extern float cont_cambio_lin;
extern int cont_dist;
extern char button_load, button_b1, button_b2;
extern char blinky;
extern char modo;
extern char dist_izq, dist_der;

// Variables para el control del rastreo
extern unsigned int ADCresult[S];
extern unsigned int ADCdist;

extern unsigned int umbral[S];
extern unsigned int blanco[S];
extern unsigned int negro[S];

extern char giro, giro_ant;
extern int negros;
extern char lin;

// Variables para el control de velocidad
extern float v_curva;
extern float v_recta;
extern float v_recta_larga;
extern float v_motores;

extern uint16_t virtual_line_position;

extern float sensor_val[S];
extern float sensor_slope[S];
extern float sensor_gain[S];
extern float sensor_offset[S];
extern float sensor_gain_ref;
extern float sensor_offset_ref;

// Prioridades de interrupciones
#define CAPTURE_PRIO	0		// Prioridad del timer en modo capture para los encoders
#define UART_PRIO		1		// Prioridad de la uart
#define MATCH_PRIO		2		// Prioridad del timer en modo match para el control peri�dico

// Sensores
extern struct analog LINE_1;	// N�mero y canal de ADC para el sensor de l�nea 1
extern struct analog LINE_2;	// N�mero y canal de ADC para el sensor de l�nea 2
extern struct analog LINE_3;	// N�mero y canal de ADC para el sensor de l�nea 3
extern struct analog LINE_4;	// N�mero y canal de ADC para el sensor de l�nea 4
extern struct analog LINE_5;	// N�mero y canal de ADC para el sensor de l�nea 5
extern struct analog LINE_6;	// N�mero y canal de ADC para el sensor de l�nea 6
extern struct data LED_ON;		// Puerto y pin para la activaci�n de los sensores

extern struct analog DIST;		// Sensor de distancia frontal
extern struct data DIST_IZQ;	// Sensor de distancia izquierdo
extern struct data DIST_DER;	// Sensor de distancia derecho

// Bater�a
extern struct analog BAT_ADC;	// N�mero y canal de ADC para la bater�a

// Pulsadores
extern struct data BUTTON_LOAD;	// Puerto y pin del pulsador de load
extern struct data BUTTON_B1;	// Puerto y pin del pulsador B1
extern struct data BUTTON_B2;	// Puerto y pin del pulsador B2

// Leds
extern struct data LED_1;		// Puerto y pin del led 1
extern struct data LED_2;		// Puerto y pin del led 2

// Motores
extern struct data MI_pos;		// Control de Puente en H +, rama izquierda
extern struct data MI_neg;		// Control de Puente en H -, rama izquierda
extern struct data MD_pos;		// Control de Puente en H +, rama derecha
extern struct data MD_neg;		// Control de Puente en H -, rama derecha

#define MI_PWM			PWM4	// Canal de PWM de los motores izquierdos
#define MD_PWM			PWM6	// Canal de PWM de los motores derechos

extern struct motor MOTOR_IZQ;
extern struct motor MOTOR_DER;

// Timers
#define MATCH_TIMER		TIMER0		// Timer a habilitar como MATCH
#define BATTERY_CH		MATCH0_0	// Canal de MATCH para ver el nivel de bater�a
#define PERIODIC_CH		MATCH0_1	// Canal de MATCH de la interrupci�n peri�dica

#define CAPTURE_TIMER	TIMER1		// Timer a habilitar como CAPTURE
extern struct data ENC_IZQ_CAP;		// Puerto y pin del encoder izquierdo (capture)
extern struct data ENC_IZQ_SIG;		// Puerto y pin del encoder izquierdo (sentido)
#define ENC_IZQ_CH	CAPTURE1_0		// Canal de CAPTURE del encoder izquierdo
extern struct data ENC_DER_CAP;		// Puerto y pin del encoder derecho (capture)
extern struct data ENC_DER_SIG;		// Puerto y pin del encoder derecho (sentido)
#define ENC_DER_CH	CAPTURE1_2		// Canal de CAPTURE del encoder derecho

// Propiedades mec�nicas
#define DIAMETER		27			// Di�metro de las ruedas en mil�metros
#define EDGE_NUM		40			// Flancos de encoder en una vuelta de la rueda


// Funci�n general de configuraci�n
void config_PUMATRON_V2_2(void);

// Funciones de configuraci�n de los dispositivos
void config_LINE_SENSORS(void);
void config_DIST_SENSORS(void);
void config_BATTERY_ADC(void);
void config_ADC(void);
void config_BUTTONS(void);
void config_LCD(void);
void config_LEDS(void);
void config_MOTORS(void);
void config_UART(void);
void config_CAPTURE(void);
void config_PERIODIC_CONTROL(void);

// Funciones de control
void selec_salida(void);
void selec_velocidad(void);
void leer_pulsadores(void);
void leer_sensores(void);
void estimar_umbrales(void);
void representar_sensores(void);
void representar_estado(void);
void actualizar(void);


#endif
