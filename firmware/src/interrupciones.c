#ifndef INTERRUPCIONES_C
#define INTERRUPCIONES_C

#include <lpc213x.h>
#include <stdlib.h>
#include <stdio.h>
#include "LPClib.h"
#include "target.h"
#include "declaraciones.h"
#include "PID.h"


void EINT_0(void)__irq
{
}

void EINT_1(void)__irq
{
}

void EINT_2(void)__irq
{
}

void EINT_3(void)__irq
{
}




void TIMER0_MATCH(void)__irq
{
	if(T0IR&(1<<0))
	{

		T0IR |= 1<<0;		// Activar flag de interrupci�n del Match0.0
	}
	else if(T0IR&(1<<1))
	{
		leer_pulsadores();

		if(ESTADO == Seleccion_salida)
		{
			selec_salida();

			if(button_b2 == ON)
			{
				ESTADO = Seleccion_velocidad;
				LED_set(LED_1, OFF);
				LED_set(LED_2, OFF);
			}
		}
		else if(ESTADO == Seleccion_velocidad)
		{
			selec_velocidad();

			if(button_b1 == ON)
				ESTADO = Parado;
		}
		else if(ESTADO == Parado)
		{
			if(button_load == ON)
			{
				GPIO_set(LED_ON, ON);	// Activar sensores

				for(i=0; i<S; i++)
				{
					blanco[i] = 1023;
					negro[i] = 0;
				}
				ESTADO = Umbralizar_sensores;
				cont2 = 0;

				LED_set(LED_1, ON);
				LED_set(LED_2, OFF);
			}
			else
			{
				if(button_b2 == ON && start < 0.5)
				{
					GPIO_set(LED_ON, ON);	// Activar sensores
					start += PER_PERIOD;
				}
				else if(button_b2 == ON && start >= 0.5)
				{
					LED_set(LED_2, ON);
				}
				else if(button_b2 == OFF && start >= 0.5)
				{
					LED_set(LED_2, OFF);

					//velocidad = 30;
					giro = '0';

					integral = 0;
					last_proportional = 0;

					if(seg_espera == 0)
						ESTADO = Rastrear;
					else
					{
						cont = 0;
						ESTADO = Espera;
					}
				}
				else
				{
					GPIO_set(LED_ON, OFF);	// Desactivar sensores
					start = 0;
				}

				if(start < 0.5)
				{
					if(cont > 0.2)
					{
						if(t == 1)
						{
							LED_set(LED_2, OFF);
							t = 0;
						}
						else
						{
							LED_set(LED_2, ON);
							t = 1;
						}
						cont = 0;
					}

					cont += PER_PERIOD;
				}
			}
		}
		else if(ESTADO == Umbralizar_sensores)
		{
			leer_sensores();
			estimar_umbrales();

			if(button_b1 == ON)
			{
				if(cont2 >= 0.2)
				{
					GPIO_set(LED_ON, OFF);	// Desactivar sensores
					ESTADO = Parado;
					start = 0;
				}
				else // cont2 < 0.2
					cont2 += PER_PERIOD;
			}
			else
				cont2 = 0;
		}
		else if(ESTADO == Espera)
		{
			cont += PER_PERIOD;
			if(cont >= seg_espera)
				ESTADO = Rastrear;
		}
		else
		{
			// Digitalizar el resultado	de la conversi�n ADC
			leer_sensores();

			// Ejecutar seguimiento
			loop_PID();

			// Actualizar variables
			//actualizar();

			// Comprobar estado del pulsador de stop
			if(button_b2 == ON)
			{
				cont2 += PER_PERIOD;
				if(cont2 >= 0.2)
				{
					MOTOR_SPEED_set(MOTOR_IZQ, v_motores, FREE);
					MOTOR_SPEED_set(MOTOR_DER, v_motores, FREE);
					PWMLER = (1<<0)|(1<<MI_PWM)|(1<<MD_PWM);

					LED_set(LED_1, OFF);
					LED_set(LED_2, OFF);
					GPIO_set(LED_ON, OFF);	// Desactivar sensores

					ESTADO = Parado;
					start = 0;
					lin = DESCONOCIDO;
				}
			}
			else
				cont2 = 0;
		}

//		// Comprobaci�n del nivel de bater�a
//		if(cont_bat >= BAT_PERIOD)
//		{
//			cont_bat = 0;
//			blinky = LIPO_TENSION_read(BAT_ADC, 7.6/8.4*2.88);
//		}
//
//		cont_bat += PER_PERIOD;
//
//		// Led parpadeando con bater�a baja
//		if(blinky == '1')
//		{
//			cont3 += PER_PERIOD;
//			if(cont3 < 0.1)
//				LED_set(LED_1, OFF);
//			else if(cont3 >= 0.1)
//			{
//				LED_set(LED_1, ON);
//
//				if(cont3 >= 0.2)
//					cont3 = 0;
//			}
//		}

		T0IR |= 1<<1;		// Activar flag de interrupci�n del Match0.1
	}
	else if(T0IR&(1<<2))
	{

		T0IR |= 1<<2;		// Activar flag de interrupci�n del Match0.2
	}
	else if(T0IR&(1<<3))
	{

		T0IR |= 1<<3;		// Activar flag de interrupci�n del Match0.3
	}


	VICVectAddr = 0;
}

void TIMER1_MATCH(void)__irq
{
	if(T1IR&(1<<0))
	{

		T1IR |= 1<<0;		// Activar flag de interrupci�n del Match1.0
	}
	else if(T1IR&(1<<1))
	{

		T1IR |= 1<<1;		// Activar flag de interrupci�n del Match1.1
	}
	else if(T1IR&(1<<2))
	{

		T1IR |= 1<<2;		// Activar flag de interrupci�n del Match1.2
	}
	else if(T1IR&(1<<3))
	{

		T1IR |= 1<<3;		// Activar flag de interrupci�n del Match1.3
	}

	VICVectAddr = 0;
}

void TIMER0_CAPTURE(void)__irq
{
	if(T0IR&(1<<4))
	{

		T0IR |= 1<<4;		// Activar flag de interrupci�n del Capture0.0
	}
	else if(T0IR&(1<<5))
	{

		T0IR |= 1<<5;		// Activar flag de interrupci�n del Capture0.1
	}
	else if(T0IR&(1<<6))
	{

		T0IR |= 1<<6;		// Activar flag de interrupci�n del Capture0.2
	}
	else if(T0IR&(1<<7))
	{

		T0IR |= 1<<7;		// Activar flag de interrupci�n del Capture0.3
	}

	VICVectAddr = 0;
}

extern unsigned long encoder_izq_cont;
extern unsigned long encoder_der_cont;

void TIMER1_CAPTURE(void)__irq
{
	if(T1IR&(1<<4))
	{
		encoder_izq_cont++;
		T1IR |= 1<<4;		// Activar flag de interrupci�n del Capture1.0
	}
	if(T1IR&(1<<6))
	{
		encoder_der_cont++;
		T1IR |= 1<<6;		// Activar flag de interrupci�n del Capture1.2
	}

	VICVectAddr = 0;
}

extern unsigned int contador_rectas, contador_curvas_der, contador_curvas_izq, contador_curvas_alternas;
extern unsigned int contador_vueltas_abs, contador_vueltas_rel;
extern int debug_pid;
extern int slope_sign;

void UART0(void)__irq
{
	char uart0_dato;

	switch(U0IIR & 0x0E)
	{
		case 0x04:	// Recepci�n
			uart0_dato = U0RBR;

			if(uart0_dato == 'l')
			{
				sprintf(cadena, "%4u %4u %4u %4u %4u %4u\n\r", ADCresult[0], ADCresult[1], ADCresult[2], ADCresult[3],
					ADCresult[4], ADCresult[5]);
				UART0_send(cadena);
			}
			else if(uart0_dato == 'u')
			{
				sprintf(cadena, "%4u %4u %4u %4u %4u %4u\n\r%4u %4u %4u %4u %4u %4u\n\r%4u %4u %4u %4u %4u %4u\n\r", blanco[0],
					blanco[1], blanco[2], blanco[3], blanco[4], blanco[5], umbral[0], umbral[1], umbral[2], umbral[3], umbral[4],
					umbral[5], negro[0], negro[1], negro[2], negro[3], negro[4], negro[5]);
				UART0_send(cadena);
			}
			else if(uart0_dato == 'd')
			{
				sprintf(cadena, "%d\n\r", ADCdist);
				UART0_send(cadena);
			}
			else if(uart0_dato == 'm')
			{
				sprintf(cadena, "%f\n\r", v_motores);
				UART0_send(cadena);
			}
			else if(uart0_dato == 't')
			{
				sprintf(cadena, "v=%2d (rel %2d), r=%2d, i=%2d, d=%2d\n\r",
					contador_vueltas_abs, contador_vueltas_rel,
					contador_rectas, contador_curvas_izq, contador_curvas_der);
				UART0_send(cadena);
			}
			else if(uart0_dato == 'n')
			{
				sprintf(cadena, "%s\n\r", lin==IZQ?"IZQ":(lin==DER?"DER":(lin==CENTRO?"CENTRO":"DESCONOCIDO")));
				UART0_send(cadena);
			}
			else if(uart0_dato == 'e')
			{
				sprintf(cadena, "%d\t%d\n\r", encoder_izq_cont, encoder_der_cont);
				UART0_send(cadena);
			}
			else if(uart0_dato == 'f')
			{
				debug_pid ^= 1;
			}
			else if(uart0_dato == 'g')
			{
				sprintf(cadena, "%d, %d\n\r", proportional, slope_sign);
				UART0_send(cadena);
			}
//			else if(uart0_dato == 'b')
//			{
//				sprintf(cadena, "bat %.2f\n\r", bat_value);
//				UART0_send(cadena);
//			}

			break;
		case 0x02:	// Transmisi�n
    		if(*uart0_ptr_tx == 0)
			{
				if(uart0_msg == '1') // Quedan mensajes por enviar
				{
					uart0_ptr_tx = uart0_buf_tx[uart0_rd]; // Se busca el nuevo mensaje para enviar
					uart0_rd++;
					uart0_rd &= N_MSG_UART0-1;
					if(uart0_rd == uart0_wr)
						uart0_msg = '0';	// Se va a enviar el �ltimo mensaje del buffer
				   	U0THR = *uart0_ptr_tx;
					uart0_ptr_tx++;
				}
				else
					uart0_repose = '1';
			}
			else	// Se contin�a mandando el mensaje
				U0THR = *uart0_ptr_tx++;
	}

	VICVectAddr = 0;
}


#endif
