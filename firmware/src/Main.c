#include <lpc213x.h>
#include <stdbool.h>
#include "LPClib.h"
#include "declaraciones.h"
#include "target.h"
#include "s1d15g00.h"
#include "PID.h"


/************************ VARIABLES ************************/
// Variables auxiliares
char ESTADO = Seleccion_salida, ESTADO_RASTREO = Rastreo_normal;
char cadena[200];			// Variable auxiliar para guardar el texto a imprimir en el LCD
float start = 0;
unsigned int seg_espera = 5; /* XXX*/
float cont = 0, cont2 = 0, cont3 = 0;
int i = 0, t = 0;
char flag = '1';
float cont_sel = 0;
float cont_bat = BAT_PERIOD;
float cont_cambio_lin = 0;
int cont_dist = 0;
char button_load = OFF, button_b1 = OFF, button_b2 = OFF;
char blinky = '0';
char modo = ANALOGICO;
char dist_izq = 0, dist_der = 0;

// Variables para el control del rastreo
unsigned int ADCresult[S];		// Array con los datos del ADC ordenados
unsigned int ADCdist;

unsigned int umbral[S];			// Umbral para diferenciar 0 y 1 en la lectura del sensor
unsigned int blanco[S];
unsigned int negro[S];

char giro = '0', giro_ant;		// Variable auxiliar para no perder la l�nea. Se inicia a un valor distinto a IZQ y DER
int negros = 0;					// N�mero de sensores en negro
char lin = DESCONOCIDO;			// L�nea a seguir

// Variables para el control de velocidad
float v_curva = 60; //70; //100;//70;		// Control de velocidad de rastreo normal. 1 max
float v_recta = 60; //60; //80;//100;//80;		// Velocidad de rastreo
float v_recta_larga = 60; //60; //80;//100;//80;		// Velocidad de rastreo
float v_motores = 0;		// XXX se inicia al 60% para arrancar con sobre impulso

//Variables PID
long sensors[S];
long sensors_average = 0;
int sensors_sum = 0;
int position = 0;
int proportional = 0;
long integral = 0;
long integral_max = 1000;
int derivative = 0;
int last_proportional = 0;
int control_value = 0;
int max_difference = 3000;
float Kp = 0.6;//1;
float Ki = 0;
float Kd = 85; //68


// Sensores
struct analog LINE_1	= {ADC_1,ADC_CH4};	// N�mero y canal de ADC para el sensor de l�nea 1
struct analog LINE_2	= {ADC_1,ADC_CH7};	// N�mero y canal de ADC para el sensor de l�nea 2
struct analog LINE_3	= {ADC_0,ADC_CH4};	// N�mero y canal de ADC para el sensor de l�nea 3
struct analog LINE_4	= {ADC_0,ADC_CH5};	// N�mero y canal de ADC para el sensor de l�nea 4
struct analog LINE_5	= {ADC_0,ADC_CH1};	// N�mero y canal de ADC para el sensor de l�nea 5
struct analog LINE_6	= {ADC_0,ADC_CH0};	// N�mero y canal de ADC para el sensor de l�nea 6
struct data LED_ON		= {0,23};			// Puerto y pin para la activaci�n de los sensores

struct analog DIST		= {ADC_1,ADC_CH6};	// Sensor de distancia frontal
struct data DIST_IZQ	= {1,23};			// Sensor de distancia izquierdo
struct data DIST_DER	= {1,21};			// Sensor de distancia derecho

// Bater�a
struct analog BAT_ADC	= {ADC_1,ADC_CH3};	// N�mero y canal de ADC para la bater�a

// Pulsadores
struct data BUTTON_LOAD	= {0,14};	// Puerto y pin del pulsador de load
struct data BUTTON_B1	= {1,19};	// Puerto y pin del pulsador B1
struct data BUTTON_B2	= {1,18};	// Puerto y pin del pulsador B2

// Leds
struct data LED_1	= {1,16};		// Puerto y pin del led 1
struct data LED_2	= {1,17};		// Puerto y pin del led 2

// Motores
struct data MI_pos	= {1,26};		// Control de Puente en H +, rama izquierda
struct data MI_neg	= {1,25};		// Control de Puente en H -, rama izquierda
struct data MD_pos	= {1,27};		// Control de Puente en H +, rama derecha
struct data MD_neg	= {1,28};		// Control de Puente en H -, rama derecha

struct motor MOTOR_IZQ	= {MI_PWM,{1,26},{1,25}};
struct motor MOTOR_DER	= {MD_PWM,{1,27},{1,28}};

// Encoders
struct data ENC_IZQ_CAP	= {0,10};	// Puerto y pin del encoder izquierdo (capture)
struct data ENC_IZQ_SIG	= {0,11};	// Puerto y pin del encoder izquierdo (sentido)
struct data ENC_DER_CAP	= {0,17};	// Puerto y pin del encoder derecho (capture)
struct data ENC_DER_SIG	= {0,18};	// Puerto y pin del encoder derecho (sentido)


/************************ FUNCIONES DE CONFIGURACI�N ************************/
void config_PUMATRON_V2_2(void)
{
	config_ADC();
	config_BUTTONS();
//	config_LCD();
	config_LEDS();
	config_MOTORS();
	config_UART();
	config_CAPTURE();
	config_PERIODIC_CONTROL();
}

void config_LINE_SENSORS(void)
{
	ADC_CH_config(LINE_1);
	ADC_CH_config(LINE_2);
	ADC_CH_config(LINE_3);
	ADC_CH_config(LINE_4);
	ADC_CH_config(LINE_5);
	ADC_CH_config(LINE_6);
	GPIO_config(LED_ON, OUT);
}

void config_DIST_SENSORS(void)
{
	ADC_CH_config(DIST);
	GPIO_config(DIST_IZQ, IN);
	GPIO_config(DIST_DER, IN);
}

void config_BATTERY_ADC(void)
{
	ADC_CH_config(BAT_ADC);
}

void config_ADC(void)
{
	config_LINE_SENSORS();
	config_DIST_SENSORS();
	config_BATTERY_ADC();

	ADC_enable(ADC_0);
	ADC_enable(ADC_1);
}

void config_BUTTONS(void)
{
	GPIO_config(BUTTON_LOAD, IN);
	GPIO_config(BUTTON_B1, IN);
	GPIO_config(BUTTON_B2, IN);
}

void config_LCD(void)
{
	LCD_BL_DIR();			// BL = Output
	LCD_CS_DIR();			// CS = Output
	LCD_SCLK_DIR();			// SCLK = Output
	LCD_SDATA_DIR();		// SDATA = Output
	LCD_RESET_DIR();		// RESET = Output
	LCD_SCLK_LOW();			// Standby SCLK
	LCD_CS_HIGH();			// Disable CS
	LCD_SDATA_HIGH();		// Standby SDATA
	LCD_BL_HIGH();			// Black Light ON = 100%

	InitLcd(); 				// Initial LCD
	LCDClearScreen();		// Limpiar pantalla
}

void config_LEDS(void)
{
	GPIO_config(LED_1, OUT);
	GPIO_config(LED_2, OUT);
}

void config_MOTORS(void)
{
	MOTOR_config(MOTOR_IZQ);
	MOTOR_config(MOTOR_DER);

	PWM_enable(PWM_PERIOD);
}

void config_UART(void)
{
	UART0_config(38400, UART_PRIO);
}


void config_CAPTURE(void)
{
	CAPTURE_CHANNEL_config(ENC_IZQ_CH, ENC_IZQ_CAP, DOUBLE_EDGE);
	GPIO_config(ENC_IZQ_SIG, IN);
	CAPTURE_CHANNEL_config(ENC_DER_CH, ENC_DER_CAP, DOUBLE_EDGE);
	GPIO_config(ENC_DER_SIG, IN);

	TIMER_config(CAPTURE_TIMER, CAPTURE_MODE, CAPTURE_PRIO);
}

void config_PERIODIC_CONTROL(void)
{
	MATCH_CHANNEL_config(PERIODIC_CH, PER_PERIOD);
	TIMER_config(MATCH_TIMER, MATCH_MODE, MATCH_PRIO);
}

extern int debug_print_counter;
extern char debug_print_buff[128];
extern int debug_pid;


/************************** FUNCI�N MAIN **************************/
int main (void)
{
	// Actualizar umbrales
	for(i=0; i<S; i++)
	{
		blanco[i] = 50;
		umbral[i] = 400;
		negro[i] = 800;
	}

	config_PUMATRON_V2_2();

	LED_set(LED_1, OFF);
	LED_set(LED_2, OFF);

	while(1) // Bucle infinito para la ejecuci�n del programa
	{
		if (debug_print_counter) {
			UART0_send(debug_print_buff);
			debug_print_counter = 0;
		}
	}
}
