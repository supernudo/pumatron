# Pumatrón

<p align="center">
<img src="images/Pumatron.jpg" width="600" align = "center">
</p>


## Authors

- Rubén Espino San José
- Javier Baliñas Santos


## License

<p align="center">

<img src="license/by-nc-sa.png" width="300" align = "center">

</p>

_All these products are released under [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/)._
